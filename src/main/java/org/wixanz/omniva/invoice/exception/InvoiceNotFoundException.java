package org.wixanz.omniva.invoice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Invoice not found")
public class InvoiceNotFoundException extends RuntimeException {
  public InvoiceNotFoundException() {
  }

  public InvoiceNotFoundException(String message) {
    super(message);
  }
}
