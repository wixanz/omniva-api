package org.wixanz.omniva.invoice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.wixanz.omniva.invoice.domain.Invoice.PaymentStatus;
import org.wixanz.omniva.invoice.exception.InvoiceNotFoundException;
import org.wixanz.omniva.invoice.service.InvoiceService;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

  @Autowired
  private InvoiceService invoiceService;

  @GetMapping(path = "/{invoiceId}/status")
  @ResponseStatus(HttpStatus.OK)
  public PaymentStatus retrievePaymentStatus(@PathVariable String invoiceId) {
    PaymentStatus paymentStatus = invoiceService.retrievePaymentStatus(invoiceId);
    if (paymentStatus == null) {
      throw new InvoiceNotFoundException();
    }
    return paymentStatus;
  }
}
