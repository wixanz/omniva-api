package org.wixanz.omniva.invoice.filter;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.springframework.stereotype.Component;
import org.wixanz.omniva.invoice.domain.Invoice;

import java.nio.charset.Charset;
import java.util.List;

@Component
public class BloomFilterManager {

  private int expectedInsertions=500000000;

  private double ffp=0.01;

  private BloomFilter<String> bloomFilter;

  public BloomFilterManager() {
    this.bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), expectedInsertions, ffp);
  }

  public void populate(List<Invoice> invoices) {
    invoices.stream().map(Invoice::getId).forEach(i -> bloomFilter.put(i));
  }

  public BloomFilter<String> getBloomFilter(){
    return bloomFilter;
  }
}
