package org.wixanz.omniva.invoice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wixanz.omniva.invoice.domain.Invoice;
import org.wixanz.omniva.invoice.filter.BloomFilterManager;
import org.wixanz.omniva.invoice.repo.InvoiceRepository;

@Service
public class InvoiceService implements IInvoiceService {

  @Autowired
  private BloomFilterManager bloomFilterManager;

  @Autowired
  private InvoiceRepository invoiceRepository;

  @Override
  public Invoice.PaymentStatus retrievePaymentStatus(String invoiceId) {
    if (!bloomFilterManager.getBloomFilter().mightContain(invoiceId))
      return null;

    return invoiceRepository.findOne(invoiceId).getPaymentStatus();
  }
}
