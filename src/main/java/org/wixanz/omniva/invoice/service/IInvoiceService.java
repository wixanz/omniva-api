package org.wixanz.omniva.invoice.service;

import org.wixanz.omniva.invoice.domain.Invoice;

public interface IInvoiceService {
  Invoice.PaymentStatus retrievePaymentStatus(String invoiceId);
}
