package org.wixanz.omniva.invoice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wixanz.omniva.invoice.domain.Invoice;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, String> {
}

