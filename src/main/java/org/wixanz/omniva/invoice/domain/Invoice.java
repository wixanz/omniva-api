package org.wixanz.omniva.invoice.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "invoices", indexes = @Index(name = "IDX_PAYMENT_STATUS", columnList = "payment_status"))
public class Invoice {

  @Id
  @GenericGenerator(
          name = "assigned-sequence",
          strategy = "org.wixanz.omniva.invoice.domain.generator.StringSequenceIdentifier",
          parameters = {
                  @Parameter(name = "sequence_name", value = "INVOICE_ID_SEQ"),
                  @Parameter(name = "min_digits", value = "15"),
                  @Parameter(name = "max_digits", value = "25")
          }
  )
  @GeneratedValue(generator = "assigned-sequence", strategy = GenerationType.SEQUENCE)
  private String id;

  @Column(name = "customer_id", nullable = false)
  private Long customerId;

  @NotNull
  private String description;

  @NotNull
  private Integer unit;

  @Column(name = "unit_price", nullable = false)
  private BigDecimal unitPrice;

  @Column(name = "payment_status", nullable = false)
  private PaymentStatus paymentStatus;

  @Column(name = "created_date", nullable = false, updatable = false)
  @Temporal(TemporalType.DATE)
  private Date createdDate;

  @Column(name = "due_date", nullable = false)
  @Temporal(TemporalType.DATE)
  private Date dueDate;

  public Invoice() {
  }

  public Invoice(Long customerId, String description, Integer unit, BigDecimal unitPrice, PaymentStatus status, Date
          dueDate) {
    this.customerId = customerId;
    this.description = description;
    this.unit = unit;
    this.unitPrice = unitPrice;
    this.paymentStatus = status;
    this.dueDate = dueDate;
    this.createdDate = new Date();
  }

  public String getId() {
    return id;
  }

  public Long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getUnit() {
    return unit;
  }

  public void setUnit(Integer unit) {
    this.unit = unit;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public PaymentStatus getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatus paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public enum PaymentStatus {
    UNPAID, PAID
  }
}
