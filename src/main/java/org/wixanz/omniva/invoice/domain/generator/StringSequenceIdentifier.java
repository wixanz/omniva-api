package org.wixanz.omniva.invoice.domain.generator;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Properties;

import static org.hibernate.id.enhanced.SequenceStyleGenerator.*;

public class StringSequenceIdentifier implements IdentifierGenerator, Configurable {

  private Integer min;
  private Integer max;
  private String sequenceCallSyntax;

  @Override
  public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
    min = Integer.valueOf(params.getProperty("min_digits"));
    max = Integer.valueOf(params.getProperty("max_digits"));

    final JdbcEnvironment jdbcEnvironment = serviceRegistry.getService(JdbcEnvironment.class);
    final Dialect dialect = jdbcEnvironment.getDialect();

    final String sequencePerEntitySuffix = ConfigurationHelper.getString(CONFIG_SEQUENCE_PER_ENTITY_SUFFIX, params,
            DEF_SEQUENCE_SUFFIX);

    final String defaultSequenceName = ConfigurationHelper.getBoolean(CONFIG_PREFER_SEQUENCE_PER_ENTITY, params, false)
            ? params.getProperty(JPA_ENTITY_NAME) + sequencePerEntitySuffix
            : DEF_SEQUENCE_NAME;

    sequenceCallSyntax = dialect.getSequenceNextValString(ConfigurationHelper.getString(SEQUENCE_PARAM, params,
            defaultSequenceName));
  }

  @Override
  public Serializable generate(SessionImplementor session, Object obj) {
    BigInteger minId = BigInteger.TEN.pow(min - 1);
    BigInteger maxId = BigInteger.TEN.pow(max).subtract(BigInteger.ONE);

    long seqValue = ((Number) Session.class.cast(session)
            .createSQLQuery(sequenceCallSyntax)
            .uniqueResult()).longValue();

    BigInteger newId = minId.add(BigInteger.valueOf(seqValue));

    if (newId.compareTo(maxId) >= 0) {
      throw new HibernateException("ID reached max digits limit");
    }

    return newId.toString();
  }
}