package org.wixanz.omniva;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.wixanz.omniva.invoice.domain.Invoice;
import org.wixanz.omniva.invoice.filter.BloomFilterManager;
import org.wixanz.omniva.invoice.repo.InvoiceRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DataGenerator implements ApplicationRunner {

  @Autowired
  BloomFilterManager bloomFilterManager;

  @Autowired
  InvoiceRepository invoiceRepository;

  @Value("${fake.data.count}")
  private Integer fakeDataCount;

  private List<Invoice> invoices = new ArrayList<>();

  @Override
  public void run(ApplicationArguments args) {

    generateFakeInvoices();
    invoices = invoiceRepository.save(invoices);
    bloomFilterManager.populate(invoices);
  }

  private void generateFakeInvoices() {
    Faker faker = new Faker();
    Invoice.PaymentStatus[] paymentStatuses = Invoice.PaymentStatus.values();

    for (int i = 0; i < fakeDataCount; i++) {

      Invoice invoice = new Invoice(
              faker.number().randomNumber(),
              faker.lorem().word(),
              faker.number().randomDigit(),
              new BigDecimal(Math.random()),
              paymentStatuses[faker.number().numberBetween(0, paymentStatuses.length)],
              faker.date().between(new Date(), DateUtils.addDays(new Date(), 13))
      );
      invoices.add(invoice);
    }
  }
}