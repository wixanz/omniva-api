package org.wixanz.omniva.unit;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.wixanz.omniva.invoice.domain.Invoice;
import org.wixanz.omniva.invoice.filter.BloomFilterManager;
import org.wixanz.omniva.invoice.repo.InvoiceRepository;
import org.wixanz.omniva.invoice.service.InvoiceService;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class InvoiceServiceTest {

  @TestConfiguration
  static class InvoiceServiceTestContextConfiguration {
    @Bean
    public InvoiceService invoiceService() {
      return new InvoiceService();
    }

    @Bean
    public BloomFilterManager bloomFilterManager() {
      return new BloomFilterManager();
    }
  }

  @Autowired
  private BloomFilterManager bloomFilterManager;

  @Autowired
  private InvoiceService invoiceService;

  @Autowired
  private InvoiceRepository invoiceRepository;

  @Test
  public void retrievePaymentStatusReturnsPaidStatus() {
    Faker faker = new Faker();

    Invoice invoice = invoiceRepository.save(new Invoice(
            faker.number().randomNumber(),
            faker.lorem().word(),
            faker.number().randomDigit(),
            new BigDecimal(Math.random()),
            Invoice.PaymentStatus.PAID,
            faker.date().between(new Date(), DateUtils.addDays(new Date(), 13))
    ));

    bloomFilterManager.getBloomFilter().put(invoice.getId());

    Invoice.PaymentStatus actualPaymentStatus = invoiceService.retrievePaymentStatus(invoice.getId());

    assertEquals(Invoice.PaymentStatus.PAID, actualPaymentStatus);
  }

  @Test
  public void retrievePaymentStatusReturnsNull() {
    Invoice.PaymentStatus actualPaymentStatus = invoiceService.retrievePaymentStatus("");

    assertEquals(null, actualPaymentStatus);
  }

}
