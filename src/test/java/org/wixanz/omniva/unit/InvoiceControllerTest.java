package org.wixanz.omniva.unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.wixanz.omniva.invoice.controller.InvoiceController;
import org.wixanz.omniva.invoice.domain.Invoice;
import org.wixanz.omniva.invoice.service.InvoiceService;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(InvoiceController.class)
public class InvoiceControllerTest {

  private final String INVOICE_ID = "1";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private InvoiceService invoiceService;

  @Test
  public void retrieveInvoicePaymentStatusIsOk() throws Exception {
    given(this.invoiceService.retrievePaymentStatus(INVOICE_ID))
            .willReturn(Invoice.PaymentStatus.PAID);
    this.mockMvc.perform(get("/api/invoice/{invoiceId}/status", INVOICE_ID).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(content().string("\"" + Invoice.PaymentStatus.PAID.name() + "\""));
  }

  @Test
  public void retrieveInvoicePaymentStatusNotFound() throws Exception {
    given(this.invoiceService.retrievePaymentStatus(INVOICE_ID)).willReturn(null);
    this.mockMvc.perform(get("/api/invoice/1/status").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
  }
}

