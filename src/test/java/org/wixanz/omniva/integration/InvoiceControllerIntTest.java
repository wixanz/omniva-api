package org.wixanz.omniva.integration;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.wixanz.omniva.App;
import org.wixanz.omniva.invoice.domain.Invoice;
import org.wixanz.omniva.invoice.filter.BloomFilterManager;
import org.wixanz.omniva.invoice.repo.InvoiceRepository;

import java.math.BigDecimal;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = App.class)
@AutoConfigureMockMvc
public class InvoiceControllerIntTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private InvoiceRepository invoiceRepository;

  @Autowired
  private BloomFilterManager bloomFilterManager;

  @Test
  public void retrieveInvoicePaymentStatusIsOk()
          throws Exception {

    Invoice testInvoice = createTestInvoice();

    mockMvc.perform(get("/api/invoice/{invoiceId}/status", testInvoice.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(content().string("\"" + testInvoice.getPaymentStatus().name() + "\""));
  }

  private Invoice createTestInvoice() {
    Faker faker = new Faker();

    Invoice invoice = invoiceRepository.save(new Invoice(
            faker.number().randomNumber(),
            faker.lorem().word(),
            faker.number().randomDigit(),
            new BigDecimal(Math.random()),
            Invoice.PaymentStatus.PAID,
            faker.date().between(new Date(), DateUtils.addDays(new Date(), 13))
    ));

    bloomFilterManager.getBloomFilter().put(invoice.getId());

    return invoice;
  }
}
