# Omniva Invoice Rest Service

The application provide the following functionality:

 - show invoice payment status
   
Additional requirements to the application:

 - invoice must have assigned unique id from 10 to 25 digits
 - use Bloom filter algorithm or alternative to improve response speed

Java technology stack:

 - Java 8 
 - Spring Boot JPA with Hibernate
 - Guava
 - Apache Commons
 - Swagger
 
Database:

  - H2(in-memory database)



# Prerequisites:

Firstly, you have to install H2 database before the application usage.



# Usage:

You can use Swagger UI to make requests:

```
http://localhost:8080/swagger-ui.html
```

Or you can make request by the following link directly:

```
http://localhost:8080/api/invoice/100000000000001/status
```

H2 Console to check stored invoices in db is available by the following link:

```
http://localhost:8080/console
```

Runnable fake data generation at startup can be configured in application.properties file:
```
fake.data.count=10
```